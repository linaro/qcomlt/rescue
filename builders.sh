#!/bin/sh

set -e

# download the firmware packages
download_firmware() {
    curl -S -C - -O -L ${QCOM_LINUX_FIRMWARE}
    echo "${QCOM_LINUX_FIRMWARE_MD5}  $(basename ${QCOM_LINUX_FIRMWARE})" > MD5
    md5sum -c MD5
}

extract_firmware() {
    # Empty/zero boot image file to clear boot partition
    dd if=/dev/zero of=boot-erase.img bs=1024 count=1024

    rm -rf ${DRAGONBOARD}-bootloaders-linux
    unzip -o -j -d ${DRAGONBOARD}-bootloaders-linux $(basename ${QCOM_LINUX_FIRMWARE}) "$@"
}

# get LICENSE file (for Linux BSP)
check_license() {
    mv $1 LICENSE
    echo "${QCOM_LINUX_FIRMWARE_LICENSE_MD5}  LICENSE" > MD5
    md5sum -c MD5
}

# usage: get_build_artifact API ref job out.file
get_build_artifact() {
    rm -f "$4"
    branch=$(echo $2 | sed -e 's!/!%2F!g; s!+!%2B!g')
    curl -S -L "$1/jobs/artifacts/$branch/download?job=$3" -o "$4"
}

# usage: get_commit_url API commit
get_commit_url() {
    curl -S "$1/repository/commits?ref_name=$2&per_page=1" | jq -c '.[].web_url' | sed -e 's/"//g'
}

# usage get_boot_files <linux|aosp> <Linux|AOSP>
get_boot_files() {
    KIND="$1"
    USER_KIND="$2"

    get_build_artifact "${BOOT_TOOL_API}" "master" "build:%20%5B${DRAGONBOARD},%20${KIND}%5D" "${DRAGONBOARD}-${KIND}.zip"

    rm -rf ${DRAGONBOARD}-${KIND}
    unzip -o -j -d ${DRAGONBOARD}-${KIND} ${DRAGONBOARD}-${KIND}.zip

    BOOT_COMMIT=$(cat ${DRAGONBOARD}-${KIND}/commit)
    rm ${DRAGONBOARD}-${KIND}/commit

    COMMIT_URL=$(get_commit_url "${BOOT_TOOL_API}" "${BOOT_COMMIT}")
    BOOT_URL=$(echo ${COMMIT_URL} | sed -e 's!/commit/!/blob/!' -e 's/"//g' -e "s!\$!/${DRAGONBOARD}/${KIND}/partition.xml!g")
    echo "** \"${USER_KIND}\":${BOOT_URL}" >> parts.in

}

# usage: do_rescue <linux|aosp> <Linux|AOSP> <ufs|emmc|sd> <UFS|eMMC|SD Card>
do_rescue() {
    KIND="$1"
    USER_KIND="$2"

    STORAGE="$3"
    USER_STORAGE="$4"

    BOOTLOADER_BASE=${BASE_NAME}-bootloader-${STORAGE}
    NAME="${BOOTLOADER_BASE}-${KIND}-${CI_PIPELINE_ID}"
    rm -rf ${NAME}
    mkdir -p ${NAME}

    shift
    shift
    shift
    shift

    cp -a "$@" \
        ${NAME}

    cd ${NAME}
    md5sum * > MD5SUMS.txt
    cd -

    mkdir -p out2
    zip -r out2/${NAME}.zip ${NAME}

    echo "* *bootloader_${STORAGE}_${KIND}*: includes the bootloaders and partition table (GPT) used when booting ${USER_KIND} images from ${USER_STORAGE}" >> list.in
}

do_header() {
    cat > out2/HEADER.textile << EOF

h4. Bootloaders for ${USER_NAME}

This page provides the bootloaders packages for the ${USER_NAME}. There are several packages:
EOF

    cat list.in >> out2/HEADER.textile

    cat >> out2/HEADER.textile << EOF

Build description:
* Build URL: "$CI_JOB_URL":$CI_JOB_URL
* Linux proprietary bootloaders package: $(basename ${QCOM_LINUX_FIRMWARE})
EOF

    cat parts.in >> out2/HEADER.textile

    cat out2/HEADER.textile
}

do_publish() {
    if [ -n "${LINARO_PUBLISH}" ]
    then
        curl -S -L -o linaro-cp.py https://git.linaro.org/ci/publishing-api.git/plain/linaro-cp.py
        python3 linaro-cp.py --link-latest ./out2 ./${LINARO_PUBLISH}/${PUBLISH_NAME}/linaro/rescue/${CI_PIPELINE_ID}
    fi
}
