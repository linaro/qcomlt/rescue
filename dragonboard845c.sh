#!/bin/sh

QCOM_LINUX_FIRMWARE='https://releases.linaro.org/96boards/dragonboard845c/qualcomm/firmware/RB3_firmware_20190529180356-v4.zip'
QCOM_LINUX_FIRMWARE_MD5='ad69855a1275547b16d94a1b5405ac62'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='cbbe399f2c983ad51768f4561587f000'

ABL_LINUX_REF='release/LE.UM.3.2.2.r1-02700-sdm845.0'
ABL_AOSP_REF='release/LU.UM.1.2.1.r1-23200-QRB5165.0-v4-header'
ABL_SOC=sdm845

BASE_NAME="dragonboard-845c"
USER_NAME="Qualcomm Robotics RB3 aka. Dragonboard 845c"
PUBLISH_NAME="dragonboard845c"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE.qcom.txt" \
    "*/02-firehose_prog/prog_firehose_ddr.elf" \
    "*/04-aop/aop.mbn" \
    "*/05-BTFM/BTFM.bin" \
    "*/06-cmnlib/cmnlib*" \
    "*/07-devcfg/devcfg.mbn" \
    "*/08-dspso/dspso.bin" \
    "*/09-hyp/hyp.mbn" \
    "*/10-imagefv/imagefv.elf" \
    "*/11-keymaster/keymaster64.mbn" \
    "*/13-sec/sec.dat" \
    "*/14-storsec/storsec.mbn" \
    "*/15-tz/tz.mbn" \
    "*/16-xbl/xbl.elf" \
    "*/16-xbl/xbl_config.elf" \
    "*/40-qupv3fw/qupv3fw.elf"

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE.qcom.txt

> list.in
> parts.in

get_build_artifact "${ABL_API}" "${ABL_LINUX_REF}" build ${DRAGONBOARD}-abl_linux.zip
get_build_artifact "${ABL_API}" "${ABL_AOSP_REF}" build ${DRAGONBOARD}-abl_aosp.zip

unzip -o -d abl-linux ${DRAGONBOARD}-abl_linux.zip
unzip -o -d abl-aosp ${DRAGONBOARD}-abl_aosp.zip

echo '* Android BootLoader (ABL) source code:' >> parts.in
echo '** "ABL source code for Linux":'$(get_commit_url "${ABL_API}" $(cat abl-linux/out/commit) | sed -e 's/commit/tree/g') >> parts.in
echo '** "ABL source code for AOSP":'$(get_commit_url "${ABL_API}" $(cat abl-aosp/out/commit) | sed -e 's/commit/tree/g') >> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

# TODO: add support for manually built ABL

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-linux/zeros*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/zeros*

do_rescue linux Linux ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
        boot-erase.img \
        abl-linux/out/${ABL_SOC}/abl.elf \
        LICENSE
do_rescue aosp  AOSP  ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
        boot-erase.img \
        abl-aosp/out/${ABL_SOC}/abl.elf \
        LICENSE

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
