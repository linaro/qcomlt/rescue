#!/bin/sh

QCOM_LINUX_FIRMWARE='https://releases.linaro.org/96boards/rb2/qualcomm/firmware/RB2_firmware_20231124-v4.zip'
QCOM_LINUX_FIRMWARE_MD5='af429926586f5a897394e543b96ed867'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='cbbe399f2c983ad51768f4561587f000'

# RB2 uses the same ABL and same soc vers as RB1
ABL_LINUX_REF='release/LE.UM.5.4.1.r1-25200-QRB2210.0'
#ABL_AOSP_REF='release/LE.UM.5.4.1.r1-25200-QRB2210.0'
ABL_SOC=rb1

BASE_NAME="rb2"
USER_NAME="Qualcomm Robotics RB2"
PUBLISH_NAME="qrb4210-rb2"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE.qcom.txt" \
    "*/02-firehose_prog/prog_firehose_ddr.elf" \
    "*/03-devcfg/devcfg.mbn" \
    "*/04-dspso/dspso.bin" \
    "*/05-hyp/hyp.mbn" \
    "*/06-non-hlos/NON-HLOS.bin" \
    "*/07-storsec/storsec.mbn" \
    "*/08-tz/tz.mbn" \
    "*/09-xbl/xbl.elf" \
    "*/09-xbl/xbl_feature_config.elf" \
    "*/20-km4/km4.mbn" \
    "*/23-qupv3fw/qupv3fw.elf" \
    "*/25-multi_image/multi_image.mbn" \
    "*/28-uefi_sec/uefi_sec.mbn" \
    "*/29-imagefv/imagefv.elf" \
    "*/30-abl/abl.elf" \
    "*/34-rpm/rpm.mbn" \
    "*/35-featenabler/featenabler.mbn" \
    "*/36-apdp/apdp.mbn" \
    "*/37-BTFM/BTFM.bin" \

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE.qcom.txt

> list.in
> parts.in

get_build_artifact "${ABL_API}" "${ABL_LINUX_REF}" build ${DRAGONBOARD}-abl_linux.zip
#get_build_artifact "${ABL_API}" "${ABL_AOSP_REF}" build ${DRAGONBOARD}-abl_aosp.zip

unzip -o -d abl-linux ${DRAGONBOARD}-abl_linux.zip
#unzip -o -d abl-aosp ${DRAGONBOARD}-abl_aosp.zip

echo '* Android BootLoader (ABL) source code:' >> parts.in
echo '** "ABL source code for Linux":'$(get_commit_url "${ABL_API}" $(cat abl-linux/out/commit) | sed -e 's/commit/tree/g') >> parts.in
#echo '** "ABL source code for AOSP":'$(get_commit_url "${ABL_API}" $(cat abl-aosp/out/commit) | sed -e 's/commit/tree/g') >> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

# TODO: add support for manually built ABL

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-linux/zeros*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/zeros*

rm ${DRAGONBOARD}-bootloaders-linux/abl.elf

do_rescue linux Linux emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
        boot-erase.img \
        abl-linux/out/${ABL_SOC}/abl.elf \
        LICENSE
do_rescue aosp  AOSP  emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
        boot-erase.img \
        LICENSE

echo >> list.in
echo "**NOTE:** Don't forget to pass @--storage emmc@ to QDL if you need to use it." >> list.in

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
