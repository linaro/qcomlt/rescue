#!/bin/sh

QCOM_LINUX_FIRMWARE='http://releases.linaro.org/96boards/dragonboard410c/qualcomm/firmware/linux-board-support-package-r1034.2.1.zip'
QCOM_LINUX_FIRMWARE_MD5='25c241bfd5fb2e55e8185752d5fe92ce'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='4d087ee0965cb059f1b2f9429e166f64'

LK_EMMC_REF='release/LA.BR.1.2.7-03810-8x16.0'
#LK_RESCUE_REF='release/LA.BR.1.2.7-03810-8x16.0+rescue'
LK_SDCARD_REF='release/LA.BR.1.2.7-03810-8x16.0+sdboot'

BASE_NAME="dragonboard-410c"
USER_NAME="Dragonboard 410c"
PUBLISH_NAME="dragonboard410c"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE" \
    "*/bootloaders-linux/*" \
    "*/cdt-linux/*" \
    "*/loaders/*" \
    "*/efs-seed/*"

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE

# Empty/zero boot image file to clear boot partition
dd if=/dev/zero of=boot-erase.img bs=1024 count=1024

> list.in
> parts.in

get_build_artifact "${LK_API}" "${LK_EMMC_REF}" build emmc.zip
#get_build_artifact "${LK_API}" "${LK_RESCUE_REF}" build rescue.zip
get_build_artifact "${LK_API}" "${LK_SDCARD_REF}" build sdcard.zip

unzip -o -j -d emmc emmc.zip
#unzip -o -j -d rescue rescue.zip
unzip -o -j -d sdcard sdcard.zip

echo '* Little Kernel (LK) source code:' >> parts.in

echo '** "SD Linux boot":'$(get_commit_url "${LK_API}" $(cat sdcard/commit) | sed -e 's/commit/tree/g') >> parts.in
echo '** "eMMC Linux boot":'$(get_commit_url "${LK_API}" $(cat emmc/commit) | sed -e 's/commit/tree/g') >> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*

do_rescue linux Linux emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
	emmc/emmc_appsboot.mbn \
        boot-erase.img \
        LICENSE
do_rescue aosp  AOSP  emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
	emmc/emmc_appsboot.mbn \
        boot-erase.img \
        LICENSE
do_rescue linux Linux sd "SD Card" \
        ${DRAGONBOARD}-bootloaders-linux/[^p]* \
	sdcard/emmc_appsboot.mbn \
        LICENSE

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
