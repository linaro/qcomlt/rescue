#!/bin/sh

QCOM_LINUX_FIRMWARE='https://releases.linaro.org/96boards/rb5/qualcomm/firmware/RB5_firmware_20230804-v7.zip'
QCOM_LINUX_FIRMWARE_MD5='29d37b8f7cecbd180dfeb057f54e1d6d'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='cbbe399f2c983ad51768f4561587f000'

ABL_LINUX_REF='release/LU.UM.1.2.1.r1-23200-QRB5165.0'
ABL_AOSP_REF='release/LU.UM.1.2.1.r1-23200-QRB5165.0-v4-header'
ABL_SOC=sm8250

BASE_NAME="rb5"
USER_NAME="Qualcomm Robotics RB5"
PUBLISH_NAME="qrb5165-rb5"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE.qcom.txt" \
    "*/cdt.bin" \
    "*/02-firehose_prog/prog_firehose_ddr.elf" \
    "*/04-aop/aop.mbn" \
    "*/06-cmnlib/cmnlib.mbn" \
    "*/06-cmnlib/cmnlib64.mbn" \
    "*/07-devcfg/devcfg.mbn" \
    "*/08-dspso/dspso.bin" \
    "*/09-hyp/hyp.mbn" \
    "*/10-apdp/apdp.mbn" \
    "*/15-tz/tz.mbn" \
    "*/16-xbl/xbl.elf" \
    "*/16-xbl/xbl_config.elf" \
    "*/24-featenabler/featenabler.mbn" \
    "*/36-km4/km4.mbn" \
    "*/40-qupv3fw/qupv3fw.elf" \
    "*/44-multi_image/multi_image.mbn" \
    "*/50-spunvm/spunvm.bin" \
    "*/51-uefi_sec/uefi_sec.mbn"

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE.qcom.txt

> list.in
> parts.in

get_build_artifact "${ABL_API}" "${ABL_LINUX_REF}" build ${DRAGONBOARD}-abl_linux.zip
get_build_artifact "${ABL_API}" "${ABL_AOSP_REF}" build ${DRAGONBOARD}-abl_aosp.zip

unzip -o -d abl-linux ${DRAGONBOARD}-abl_linux.zip
unzip -o -d abl-aosp ${DRAGONBOARD}-abl_aosp.zip

echo '* Android BootLoader (ABL) source code:' >> parts.in
echo '** "ABL source code for Linux":'$(get_commit_url "${ABL_API}" $(cat abl-linux/out/commit) | sed -e 's/commit/tree/g') >> parts.in
echo '** "ABL source code for AOSP":'$(get_commit_url "${ABL_API}" $(cat abl-aosp/out/commit) | sed -e 's/commit/tree/g') >> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

# TODO: add support for manually built ABL

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-linux/zeros*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/zeros*

do_rescue linux Linux ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
        boot-erase.img \
        abl-linux/out/${ABL_SOC}/abl.elf \
        LICENSE
do_rescue aosp  AOSP  ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
        boot-erase.img \
        abl-aosp/out/${ABL_SOC}/abl.elf \
        LICENSE

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
